-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 13, 2016 at 11:06 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `333proj`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `checkGame`(IN `gName` VARCHAR(255))
    NO SQL
BEGIN
Select status 
From games 
Where game_name = gName;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `createGame`(IN `user` VARCHAR(255), IN `pass` VARCHAR(225), IN `game` VARCHAR(225))
begin
Declare ret varchar(255);
Declare temp varchar(255);
Declare temp_count Int;
Declare gid_var Int;
Declare bid_var Int;


-- check valid username and password
select username
from Users
where username=user and hashed_password = saltedHash(user, pass)
into temp;

-- check valid game name
Select Count(*) 
from games g
where g.game_name = game
Into temp_count;

-- user and pass
if temp is null then
set ret = "Invalid username or password";

-- game name
elseif temp_count > 0 THEN
set ret = "Game name already taken";

-- valid
else
INSERT
	into games (status, game_name)
    VALUES ("waiting", game);
    
Set gid_var = LAST_INSERT_ID();


INSERT
	into boards (bad_win, rejection_count, no_on_mission, good_win, gid)
    VALUES (0, 0, 0, 0, gid_var);
    
Set bid_var = LAST_INSERT_ID();

-- give game bid
UPDATE games
	Set bid = bid_var
    Where id = gid_var;
    
-- give user gid
Update users
	Set gid = gid_var
    Where username = user;
set ret = "yes";

end if;
select ret;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `joinGame`(IN `user` VARCHAR(225), IN `pass` VARCHAR(225), IN `game` VARCHAR(225))
    NO SQL
Begin
Declare ret varchar(255);
Declare temp varchar(255);
Declare temp_count Int;
Declare gid_var Int;
Declare bid_var Int;

-- check valid username and password
select username
from Users
where username=user and hashed_password = saltedHash(user, pass)
into temp;

-- check valid game name and able to join
Select Count(*) 
from games g
where g.game_name = game And g.status = 'waiting'
Into temp_count;

-- user and pass
if temp is null then
set ret = "Invalid username or password";
Select ret;
ROLLBACK;

-- game name/status
elseif temp_count = 0 THEN
set ret = "Game does not exist or has already started";
Select ret;
ROLLBACK;

-- valid
else
Select g.id
	from games g
    Where g.game_name = game
    Into gid_var;
    
-- give user gid
Update users
	Set gid = gid_var
    Where username = user;
    
-- check if over 10 players    
Select count(id) from users u
Where u.gid = gid_var
Into temp_count;

if temp_count > 10 THEN
set ret = 'Too many people in game';
select ret;
Rollback;
end if;
    
set ret = "yes";
Commit;
select ret;
end if;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `showChar`(IN `user` VARCHAR(255))
    NO SQL
Begin
Select c.character, c.description, c.cid <=2 As 'good'
From characterview c
Where c.username = user;
End$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `showOthers`(IN `userI` VARCHAR(255), IN `gName` VARCHAR(255))
    NO SQL
Begin
 Declare temp varchar(255);
  Declare charname varchar(255);
Create temporary table retTable (username varchar(255)); 

-- figure out what character this user is
 Select c.character 
 From characterview c
 Where username = userI
 Into temp;
-- Set temp= 'merlin';

-- get 1st word of name (http://stackoverflow.com/questions/3471199/mysql-get-all-characters-before-blank) and lowercase
 SELECT LCase(SUBSTRING_INDEX(temp, ' ', 1 ))
 Into charname;

-- get corresponding view, if same to name (merlin, minion, percival)
If charname = 'merlin' THEN
	Insert Into retTable (username)
	 Select username from merlin
    Where game_name = gName;
 ELSEIF charname = 'percival' THEN
 	Insert Into retTable
 	 Select username from percival
     Where game_name = gName;

-- otherwise, minion, mordred, morgana, oberon, assassin, or servant
 elseif charname = 'mordred' or charname = 'morgana' or charname = 'assassin' or charname = 'minion' THEN
   Insert Into retTable
	 Select username from minion
    Where game_name = gName;
    -- oberon and servant get nothing
-- else do nothing

end if;
Select * from retTable;
end$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `signUp`(IN `inEmail` VARCHAR(320), IN `inUsername` VARCHAR(255), IN `inPassword` VARCHAR(255))
BEGIN
DECLARE userID	INT;
DECLARE result VARCHAR(255);

SELECT id
FROM users
WHERE username	= inUsername
LIMIT 1
INTO userID;

IF userID IS NULL THEN
Insert 
  Into Users (username, hashed_password, email) 
  values (inUsername, saltedHash(inUsername, inPassword), inEmail);
    Set result = 'yes';

ELSE
SET result = 'Username already taken';

End If;

SELECT result;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `startGame`(IN `gName` VARCHAR(255), IN `merSassin` VARCHAR(5), IN `perc` VARCHAR(5), IN `mord` VARCHAR(5), IN `morg` VARCHAR(5), IN `obe` VARCHAR(5))
    NO SQL
proc: begin 
Declare ret varchar(255);
Declare playerCount Int;
Declare badNum Int;
Declare goodNum Int;
Declare numChars Int;
Declare numBadChars Int;
Declare numGoodChars Int;
Declare gid_var Int;

Set numChars = 0;
Set numBadChars = 0;
Set numGoodChars = 0;

-- get id
Select id
From games
Where game_name = gName
Into gid_var;

-- count number of special characters
if merSassin = 'true' then
	set numChars = numChars +2;
    set numBadChars = numBadChars + 1;
    set numGoodChars = numBadChars + 1;
end if;    
if perc = 'true' then
	set numChars = numChars + 1;
    set numGoodChars = numBadChars + 1;
end if;
if mord = 'true' then
	set numChars = numChars +1;
    set numBadChars = numBadChars + 1;
end if;
if morg = 'true' then
	set numChars = numChars +1;
    set numBadChars = numBadChars + 1;
end if;
if obe = 'true' then
	set numChars = numChars +1;
    set numBadChars = numBadChars + 1;
end if;                    

-- get number of players in game
Select Count(id) 
From users u
Where u.gid = gid_var
Into playerCount;

-- check if too many characters
if playerCount < numChars then
	Set ret = 'Too many powerful characters';
	Select ret;
	Leave proc;
else 

    -- determine how many good and bad chars
    if playerCount = 5 THEN
       Set goodNum = 3;
       Set badNum = 2;

    elseif playerCount = 6 THEN
       Set goodNum = 4;
       Set badNum = 2;

    elseif playerCount = 7 THEN
       Set goodNum = 4;
       Set badNum = 3;

    elseif playerCount = 8 THEN
       Set goodNum = 5;
       Set badNum = 3;

    elseif playerCount = 9 THEN
       Set goodNum = 6;
       Set badNum = 4;

    elseif playerCount = 10 THEN
       Set goodNum = 6;
       Set badNum = 4;

    else
       Set ret = "Not enough players";
	Select ret;
	Leave proc;
     end if;  
     
     -- check too many good or bad
     if goodNum < numGoodChars then
     	Set ret = "Too many powerful heroes";
	Select ret;
	Leave proc;
     elseif badNum < numBadChars then
     	Set ret = "Too many powerful villains";
	Select ret;
	Leave proc;
     else
     
     -- Set special characters
     
     -- Set merlin and assassin
     if merSassin = 'true' then
     	-- Set merlin
        Update users
     	Set cid = 0
     	Where gid = gid_var and cid is null
     	Order By RAND()
     	Limit 1;
        
      Set  goodNum = goodNum-1;
      
        -- set assassin
         Update users
     	Set cid = 3
     	Where gid = gid_var  and cid is null
     	Order By RAND()
     	Limit 1;
        
       Set badNum = badNum-1;
     end if;
     
     -- percival
     if perc = 'true' THEN
     Update users
     	Set cid = 2
     	Where gid = gid_var  and cid is null
     	Order By RAND()
     	Limit 1;
        
       Set goodNum = goodNum-1; 
     end if;
     
     -- mordred
     if mord = 'true' THEN
     Update users
     	Set cid = 5
     	Where gid = gid_var  and cid is null
     	Order By RAND()
     	Limit 1;
        
        Set badNum = badNum -1;
     end if;
     
     -- morgana
     if morg = 'true' Then
     Update users
     	Set cid = 6
     	Where gid = gid_var  and cid is null
     	Order By RAND()
     	Limit 1;
        
        Set badNum = badNum-1;
     end if;   
     -- oberon
     if obe = 'true' THEN
     Update users
     	Set cid = 7
     	Where gid = gid_var  and cid is null
     	Order By RAND()
     	Limit 1;
     end if;
     
      -- set random good   
     Update users
     Set cid = 1
     Where gid = gid_var and cid is null
     Order By RAND()
     Limit goodNum;
     	
        
  -- set rest bad   
     Update users
     Set cid = 4
     Where gid = gid_var  and cid is null
     Limit badNum;
     
     Set ret = 'yes';
     
    -- change game status
     Update games
     Set status = 'started'
    Where id = gid_var;
     
     
     end if;
end if;

SELECT ret;
end$$

--
-- Functions
--
CREATE DEFINER=`laeschjs`@`localhost` FUNCTION `saltedHash`(username	VARCHAR(255),
																											password	VARCHAR(255)) RETURNS binary(20)
    DETERMINISTIC
RETURN UNHEX(SHA1(CONCAT(username,	password)))$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `boards`
--

CREATE TABLE IF NOT EXISTS `boards` (
  `id` int(11) NOT NULL,
  `no_on_mission` int(11) NOT NULL,
  `rejection_count` int(11) NOT NULL,
  `good_win` int(11) NOT NULL,
  `bad_win` int(11) NOT NULL,
  `gid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `boards`
--

INSERT INTO `boards` (`id`, `no_on_mission`, `rejection_count`, `good_win`, `bad_win`, `gid`) VALUES
(1, 0, 0, 0, 0, 2),
(2, 0, 0, 0, 0, 3),
(3, 0, 0, 0, 0, 4),
(4, 0, 0, 0, 0, 5),
(5, 0, 0, 0, 0, 6),
(6, 0, 0, 0, 0, 7),
(7, 0, 0, 0, 0, 8),
(8, 0, 0, 0, 0, 9),
(9, 0, 0, 0, 0, 10),
(10, 0, 0, 0, 0, 11),
(11, 0, 0, 0, 0, 12),
(12, 0, 0, 0, 0, 13),
(13, 0, 0, 0, 0, 14),
(14, 0, 0, 0, 0, 15),
(15, 0, 0, 0, 0, 16);

-- --------------------------------------------------------

--
-- Table structure for table `characters`
--

CREATE TABLE IF NOT EXISTS `characters` (
  `id` int(11) NOT NULL,
  `leader` bit(1) NOT NULL,
  `on_mission` bit(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `characters`
--

INSERT INTO `characters` (`id`, `leader`, `on_mission`, `name`) VALUES
(0, b'0', b'0', 'Merlin'),
(1, b'0', b'0', 'Loyal Servant of Arthur'),
(2, b'0', b'0', 'Percival'),
(3, b'0', b'0', 'Assassin'),
(4, b'0', b'0', 'Minion of Mordred'),
(5, b'0', b'0', 'Mordred'),
(6, b'0', b'0', 'Morgana'),
(7, b'0', b'0', 'Oberon');

-- --------------------------------------------------------

--
-- Stand-in structure for view `characterview`
--
CREATE TABLE IF NOT EXISTS `characterview` (
`username` varchar(255)
,`character` varchar(255)
,`cid` int(11)
,`description` varchar(900)
);

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE IF NOT EXISTS `games` (
  `id` int(11) NOT NULL,
  `game_name` varchar(230) NOT NULL,
  `status` varchar(255) DEFAULT NULL,
  `bid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `game_name`, `status`, `bid`) VALUES
(2, 'hi', 'waiting', 1),
(3, 'bye', 'waiting', 2),
(4, 'abcd', 'waiting', 3),
(5, 'new', 'waiting', 4),
(6, 'adsf', 'waiting', 5),
(7, 'randomgame', 'started', 6),
(8, 'abdcefg', 'waiting', 7),
(9, 'abcdefg', 'waiting', 8),
(10, 'ty', 'waiting', 9),
(11, 'a', 'waiting', 10),
(12, 'z', 'started', 11),
(13, 'm', 'waiting', 12),
(14, '9999', 'started', 13),
(15, '`', 'started', 14),
(16, 'Demo', 'started', 15);

-- --------------------------------------------------------

--
-- Table structure for table `is_in`
--

CREATE TABLE IF NOT EXISTS `is_in` (
  `cid` int(11) NOT NULL,
  `gid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `merlin`
--
CREATE TABLE IF NOT EXISTS `merlin` (
`username` varchar(255)
,`game_name` varchar(230)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `minion`
--
CREATE TABLE IF NOT EXISTS `minion` (
`username` varchar(255)
,`game_name` varchar(230)
);

-- --------------------------------------------------------

--
-- Table structure for table `obeys`
--

CREATE TABLE IF NOT EXISTS `obeys` (
  `cid` int(11) NOT NULL,
  `rid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obeys`
--

INSERT INTO `obeys` (`cid`, `rid`) VALUES
(0, 0),
(0, 1),
(0, 2),
(0, 3),
(0, 4),
(0, 5),
(0, 7),
(0, 8),
(1, 0),
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 7),
(2, 0),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(2, 7),
(2, 10),
(3, 0),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 6),
(3, 7),
(3, 9),
(4, 0),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 6),
(4, 7),
(5, 0),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 6),
(5, 7),
(5, 11),
(6, 0),
(6, 1),
(6, 2),
(6, 3),
(6, 4),
(6, 6),
(6, 7),
(6, 12),
(7, 0),
(7, 1),
(7, 2),
(7, 3),
(7, 4),
(7, 6),
(7, 7),
(7, 13);

-- --------------------------------------------------------

--
-- Stand-in structure for view `percival`
--
CREATE TABLE IF NOT EXISTS `percival` (
`username` varchar(255)
,`game_name` varchar(230)
);

-- --------------------------------------------------------

--
-- Table structure for table `quests`
--

CREATE TABLE IF NOT EXISTS `quests` (
  `id` int(11) NOT NULL,
  `quest_num` int(11) NOT NULL,
  `num_questers` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quests`
--

INSERT INTO `quests` (`id`, `quest_num`, `num_questers`) VALUES
(1, 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `rules`
--

CREATE TABLE IF NOT EXISTS `rules` (
  `id` int(11) NOT NULL,
  `type_of_rule` varchar(255) DEFAULT NULL,
  `description` varchar(900) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rules`
--

INSERT INTO `rules` (`id`, `type_of_rule`, `description`) VALUES
(0, 'Objective', ''),
(1, 'Start Phase', ''),
(2, 'Gameplay', ''),
(3, 'Quests', ''),
(4, 'Voting', ''),
(5, 'Accepting', ''),
(6, 'Rejecting', ''),
(7, 'Game End', ''),
(8, 'Merlin', ''),
(9, 'Assassin', ''),
(10, 'Percival', ''),
(11, 'Mordred', ''),
(12, 'Morgana', ''),
(13, 'Oberon', '');

-- --------------------------------------------------------

--
-- Stand-in structure for view `userchargame`
--
CREATE TABLE IF NOT EXISTS `userchargame` (
`username` varchar(255)
,`cid` int(11)
,`game_name` varchar(230)
);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `hashed_password` binary(20) NOT NULL,
  `cid` int(11) DEFAULT NULL,
  `gid` int(11) DEFAULT NULL,
  `email` varchar(320) NOT NULL,
  `qid` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=70 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `hashed_password`, `cid`, `gid`, `email`, `qid`) VALUES
(1, 'testuser', 0x080d7ec382c2b063c382c2a2c383c2bb28c383c2, 6, 7, 'testuser@test.com', NULL),
(2, 'anotheruser', 0xc382c2adc383c59370c382c28d2ec383c2ae5bc3, NULL, NULL, 'anotheruser@test.com', NULL),
(3, 'somebody', 0x0cc382c2b32ec3a2e282acc2a0157ac383c28d53, NULL, NULL, 'somebody@test.com', NULL),
(4, 'jkjlkj', 0xccf2ab36330cea2f2b561b39f73d91a1b80a9228, NULL, NULL, 'joo@mlmlm.com', NULL),
(5, 'tomaa', 0xa8c0da2f3f1c4e612acdede354ab6ea462200f65, NULL, NULL, 'adfdsf22', NULL),
(6, 'tom', 0x1d5f29d807ee33b3f42481108436c7026584cb3d, NULL, NULL, 'afdfdafda', NULL),
(7, 'qwerrccc', 0x3c0a96c28fa36c2ab413fd1dcb688d64bb7906aa, NULL, NULL, 'dfafdafdafda', NULL),
(8, 'pooo', 0x81cc32284d61b2e9710a547bd603559459bbf2b9, NULL, NULL, 'pooo@jklk.ocm', NULL),
(9, 'qwertyy', 0xea2385ddb7f91b156d734deaaeabff1adeaaa123, NULL, NULL, 'jklmnop@jkjkl.com', NULL),
(10, 'wooohoo', 0xb6d08e882e73eb711365dba769115824810dc0a0, NULL, NULL, 'wooohooo@...com', NULL),
(11, 'tomfdadfa', 0x1041a20fcd5725852ec56a2e74efba0de6d07cba, NULL, NULL, 'afddaf', NULL),
(12, 'tomee', 0x6e501a7c0f1dbec157dcf1ac0fefa9bdb9665169, NULL, NULL, 'ww', NULL),
(13, 'poedude', 0x164debce60ea10bcfd63629438a9ea664aa82090, 7, 7, 'poij', NULL),
(15, 'bill', 0x2c73008ba0b824d8a40bdcb7f7e6516b0fbed979, 5, 7, 'bob@bobby.com', NULL),
(16, 'barr', 0xc08e854651c8254159f3309597eb556e212c4882, NULL, NULL, 'la@nom', NULL),
(17, 'ijojf', 0x90e0df7e765aa89740b4437fc2787c94ba4690ef, NULL, NULL, 'jifd', NULL),
(18, 'eff', 0x3e8d966e106bd081f4800cc10aa8a1f2a40401a5, NULL, NULL, 'fefef', NULL),
(19, 'yy4y', 0x33e29e738fee98bd019fbad40f3f7bf3bb6fe23b, NULL, NULL, 'efeyt', NULL),
(20, 'ukuku', 0xa7ae98b3288465a6956f72b7da9fbc57c2d1a363, NULL, NULL, 'kku', NULL),
(21, 'ukukj', 0xae5f6e8ee8a3c252781aa8a998216534baf28261, NULL, NULL, 'kuk', NULL),
(22, 'bob', 0xd7d8cb44fa724477bb1b48ea1f8529dc1cbd0a26, NULL, NULL, 'jljl', NULL),
(23, 'jkljl', 0x8e92f49bdea154b6e6c4cbd2aa00dc016713697e, 1, 7, 'jkjl', NULL),
(24, 'asdfaf', 0x5ca253a59bbb3eead1eeeecfcb88086558c878a5, 1, 7, 'asdffdsa', NULL),
(25, 'abcd', 0xe0378e12d7ac5f9af37052d8763be4f3e8d13041, NULL, 4, 'abcd', NULL),
(26, 'adsfda', 0x24b627346f4dd0c35f68c2fb30d04391f2d81ad4, NULL, NULL, 'abvc', NULL),
(27, 'hi', 0x6880aa7be2aa9880386d8c50d16adf6cd52d229b, NULL, 5, 'hi', NULL),
(28, 'adsf', 0xa7eab6aaeb77f3a73ddccbbdd44278a1e96d1814, NULL, 6, 'adsf', NULL),
(29, 'randomuser', 0x90a17836147ed34d5422a57c254445eb1e8f57c9, 3, 7, 'randomuser@user.com', NULL),
(30, 'coolguy', 0x39ae65f3461abc822395fc088856553e7473c6d8, 1, 7, 'coolguy@cool.com', NULL),
(31, 'cvcvdf', 0x47e6b93c9b90ec4236ebeb3ebfb72e4357e1c42d, NULL, NULL, 'asdfcccv', NULL),
(32, 'abcdefg', 0x9bbc2bb3533171d8e501f28d8bc075051daf6d60, NULL, 9, 'abcdefg', NULL),
(33, 'nm', 0xe874cda4844fa14a24f4835234ac27124f9c670c, 2, 7, 'nm', NULL),
(34, 'po', 0xe170f80139aac716ebca4320121de416231b4109, 1, 7, 'po', NULL),
(35, 'ad', 0x2537e872f7a38dbc275d111c571d5de3f9259834, 0, 7, 'ad', NULL),
(36, 'ty', 0x17fd3dfe874b0a661fdb965a6e2e6622d8d28f96, NULL, 10, 'ty', NULL),
(37, 'wer', 0x8799e914d696af765cb33604694a4b076db6981a, NULL, 9, 'wer', NULL),
(38, 'io', 0x440f26e1709d477316d8af0479a4eeea747ca6a6, NULL, 9, 'io', NULL),
(39, 'a', 0xe0c9035898dd52fc65c41454cec9c4d2611bfb37, 1, 11, 'a', NULL),
(40, 'b', 0x9a900f538965a426994e1e90600920aff0b4e8d2, 1, 11, 'b', NULL),
(41, 'c', 0xbdb480de655aa6ec75ca058c849c4faf3c0f75b1, 4, 11, 'c', NULL),
(42, 'd', 0x388ad1c312a488ee9e12998fe097f2258fa8d5ee, 4, 11, 'd', NULL),
(43, 'e', 0x1f444844b1ca616009c2b0e3564fecc065872b5b, 1, 11, 'e', NULL),
(44, 'z', 0xd7dacae2c968388960bf8970080a980ed5c5dcb7, 4, 12, 'z', NULL),
(45, 'zz', 0xcb990257247b592eaaed54b84b32d96b7904fd95, 1, 12, 'zz', NULL),
(46, 'zzz', 0x984ff6ee7c78078d4cb1ca08255303fb8741d986, 0, 12, 'zzz', NULL),
(47, 'zzzz', 0xfd15e5dc45839815c6465b7b7e60728057c5af3f, 2, 12, 'zzzz', NULL),
(48, 'zzzzz', 0xa11257ca53dc534da44b299c064fef3b37da6bbd, 3, 12, 'zzzzz', NULL),
(49, 'm', 0xb8d09b4d8580aacbd9efc4540a9b88d2feb9d7e5, NULL, 13, 'm', NULL),
(50, 'n', 0x07962e32beac4da179b30c06f1c1e71bd220f782, NULL, 13, 'n', NULL),
(51, '1', 0x17ba0791499db908433b80f37c5fbc89b870084b, NULL, 13, '1', NULL),
(52, '2', 0x12c6fc06c99a462375eeb3f43dfd832b08ca9e17, NULL, 13, '2', NULL),
(53, '3', 0xb6692ea5df920cad691c20319a6fffd7a4a766b8, NULL, 13, '3', NULL),
(54, '9999', 0xd528fca3b163c05703e88b5285440bec28ecf185, 0, 14, '9999', NULL),
(55, '5', 0x8effee409c625e1a2d8f5033631840e6ce1dcb64, 4, 14, '5', NULL),
(56, '6', 0x59129aacfb6cebbe2c52f30ef3424209f7252e82, 3, 14, '6', NULL),
(57, '7', 0xd321d6f7ccf98b51540ec9d933f20898af3bd71e, 2, 14, '7', NULL),
(58, '8', 0xb37f6ddcefad7e8657837d3177f9ef2462f98acf, 1, 14, '8', NULL),
(59, '`', 0x222b2b4ab2fdd2dd8ef261d8953351ac6bc457fa, 3, 15, '`', NULL),
(60, '/', 0xebbffb7d7ea5362a22bfa1bab0bfdeb1617cd610, 1, 15, '/', NULL),
(61, ';', 0x17ada2900d14cae808b76f484a73a2b599b90bce, 1, 15, ';', NULL),
(62, '.', 0x9d891e731f75deae56884d79e9816736b7488080, 7, 15, '.', NULL),
(63, '=', 0x6947818ac409551f11fbaa78f0ea6391960aa5b8, 0, 15, '=', NULL),
(64, 'JoeDemo', 0x9f91a24fbbea49ff2e7b2ed8a35ea381e7092366, 1, 16, 'joe@gmail.com', NULL),
(65, 'guy', 0x69e288cafb493a1818ba2e19ef447f4d0a1fe756, 7, 16, 'guy', NULL),
(66, 'gal', 0x7bd30139315d416f83e7deb47c43f199fbf461eb, 3, 16, 'gal', NULL),
(67, 'girl', 0x2b9677d54b9ae458f8988c94e75b6b18dc375552, 0, 16, 'girl@gily.com', NULL),
(68, 'stud', 0x9594803c3f1e38f04b6bb8e7118db3c49f8209fe, 1, 16, 'stud', NULL),
(69, 'loveit', 0xb074170e895ac46053faf41f32b4fe0e08625540, NULL, NULL, 'loveit', NULL);

-- --------------------------------------------------------

--
-- Stand-in structure for view `usersview`
--
CREATE TABLE IF NOT EXISTS `usersview` (
`username` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `votes_on`
--

CREATE TABLE IF NOT EXISTS `votes_on` (
  `uid` int(11) NOT NULL,
  `qid` int(11) NOT NULL,
  `vote` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure for view `characterview`
--
DROP TABLE IF EXISTS `characterview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `characterview` AS select `u`.`username` AS `username`,`c`.`name` AS `character`,`c`.`id` AS `cid`,`r`.`description` AS `description` from (((`users` `u` join `characters` `c` on((`u`.`cid` = `c`.`id`))) join `obeys` `o` on((`c`.`id` = `o`.`cid`))) join `rules` `r` on((`o`.`rid` = `r`.`id`))) group by `u`.`username` order by `cid`;

-- --------------------------------------------------------

--
-- Structure for view `merlin`
--
DROP TABLE IF EXISTS `merlin`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `merlin` AS select `userchargame`.`username` AS `username`,`userchargame`.`game_name` AS `game_name` from `userchargame` where ((`userchargame`.`cid` >= 3) and (`userchargame`.`cid` <> 5));

-- --------------------------------------------------------

--
-- Structure for view `minion`
--
DROP TABLE IF EXISTS `minion`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `minion` AS select `userchargame`.`username` AS `username`,`userchargame`.`game_name` AS `game_name` from `userchargame` where ((`userchargame`.`cid` >= 3) and (`userchargame`.`cid` <> 7));

-- --------------------------------------------------------

--
-- Structure for view `percival`
--
DROP TABLE IF EXISTS `percival`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `percival` AS select `userchargame`.`username` AS `username`,`userchargame`.`game_name` AS `game_name` from `userchargame` where ((`userchargame`.`cid` = 0) or (`userchargame`.`cid` = 6));

-- --------------------------------------------------------

--
-- Structure for view `userchargame`
--
DROP TABLE IF EXISTS `userchargame`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `userchargame` AS select `u`.`username` AS `username`,`u`.`cid` AS `cid`,`g`.`game_name` AS `game_name` from ((`users` `u` join `characters` `c` on((`u`.`cid` = `c`.`id`))) join `games` `g` on((`u`.`gid` = `g`.`id`))) order by `u`.`gid`;

-- --------------------------------------------------------

--
-- Structure for view `usersview`
--
DROP TABLE IF EXISTS `usersview`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `usersview` AS select `users`.`username` AS `username` from `users`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `boards`
--
ALTER TABLE `boards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `game_id_fk_index` (`gid`);

--
-- Indexes for table `characters`
--
ALTER TABLE `characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `game_name` (`game_name`),
  ADD KEY `board_id_fk_index` (`bid`);

--
-- Indexes for table `is_in`
--
ALTER TABLE `is_in`
  ADD KEY `char_id_fk_index` (`cid`),
  ADD KEY `game_id_fk_index` (`gid`);

--
-- Indexes for table `obeys`
--
ALTER TABLE `obeys`
  ADD PRIMARY KEY (`cid`,`rid`),
  ADD KEY `char_id_fk_index` (`cid`),
  ADD KEY `abilitiy_id_fk_index` (`rid`);

--
-- Indexes for table `quests`
--
ALTER TABLE `quests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rules`
--
ALTER TABLE `rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `hashed_password` (`hashed_password`),
  ADD KEY `character_id_fk_index` (`cid`),
  ADD KEY `user_game_fk` (`gid`);

--
-- Indexes for table `votes_on`
--
ALTER TABLE `votes_on`
  ADD KEY `votes_on_key` (`uid`,`qid`),
  ADD KEY `quest_votes_on_fk` (`qid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `boards`
--
ALTER TABLE `boards`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `characters`
--
ALTER TABLE `characters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `quests`
--
ALTER TABLE `quests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `rules`
--
ALTER TABLE `rules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=70;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `boards`
--
ALTER TABLE `boards`
  ADD CONSTRAINT `board_game_fk` FOREIGN KEY (`gid`) REFERENCES `games` (`id`);

--
-- Constraints for table `games`
--
ALTER TABLE `games`
  ADD CONSTRAINT `game_board_fk` FOREIGN KEY (`bid`) REFERENCES `boards` (`id`);

--
-- Constraints for table `is_in`
--
ALTER TABLE `is_in`
  ADD CONSTRAINT `char_is_in_fk` FOREIGN KEY (`cid`) REFERENCES `characters` (`id`),
  ADD CONSTRAINT `game_is_in_fk` FOREIGN KEY (`gid`) REFERENCES `games` (`id`);

--
-- Constraints for table `obeys`
--
ALTER TABLE `obeys`
  ADD CONSTRAINT `has_an_ability_fk` FOREIGN KEY (`rid`) REFERENCES `rules` (`id`),
  ADD CONSTRAINT `has_an_char_fk` FOREIGN KEY (`cid`) REFERENCES `characters` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `Users_Character_fk` FOREIGN KEY (`cid`) REFERENCES `characters` (`id`),
  ADD CONSTRAINT `user_game_fk` FOREIGN KEY (`gid`) REFERENCES `games` (`id`);

--
-- Constraints for table `votes_on`
--
ALTER TABLE `votes_on`
  ADD CONSTRAINT `quest_votes_on_fk` FOREIGN KEY (`qid`) REFERENCES `quests` (`id`),
  ADD CONSTRAINT `user_votes_on_fk` FOREIGN KEY (`uid`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
