"use strict"

var WEB_APP = "./getData.php";
var username;
var gameName;
var side;   // good or bad
var succ;   // success button
var fail;   // fail button
var appr;   // approve button
var rej;    // reject button
var leader; // yes or no (if leader)
var numOnQuest; //number of players on a quest
var goingOnQuest; // true or false for this user
var nextQuest; // will be r if rejected, g if good won, b if bad won
var assassin; // yes or no (if assassin)

window.onload = function() {
    // create button
    var createBut = document.getElementById("CG");
    if(createBut) {
        createBut.onclick = (function() {
           // var email = document.getElementById("inputEmail").value;
            username = document.getElementById('inputUserLogin').value;
            var password = document.getElementById('inputPassLogin').value;
            gameName = document.getElementById('inputGNameLogin').value;

            // console.log(gameName + " " + username + " " + password);
            $.post(WEB_APP, {
                "create": username,
                "password": password,
                "gName": gameName
            }).done(createGame).fail(ajaxFail);
        });
    }

    // Join game button
    var joinBut = document.getElementById("JG");
    if(joinBut) {
        joinBut.onclick = (function() {
           // var email = document.getElementById("inputEmail").value;
            username = document.getElementById('inputUserLogin').value;
            var password = document.getElementById('inputPassLogin').value;
            gameName = document.getElementById('inputGNameLogin').value;

            //console.log(email + " " + username + " " + password);
            $.post(WEB_APP, {
                "login": username,
                "password": password,
                "gName": gameName
            }).done(goToBoard).fail(ajaxFail);
            
        });
    }


    // sign up button
    var signBut = document.getElementById("SU");
    if(signBut){
        signBut.onclick = (function(){
            var email = document.getElementById("inputEmail").value;
            username = document.getElementById('inputUser').value;
            var password = document.getElementById('inputPass').value;

            //console.log(email + " " + username + " " + password);
            $.get(WEB_APP, {
                "email": email,
                "username": username,
                "password": password
            }).done(signUp).fail(ajaxFail);
        });
    }

    // Username header for board
    var uh = document.getElementById("headUsername");
    // console.log("hi");
    if(uh){
        // username = window.location.hash.substring(1);
        begin(uh);
        // console.log("ho");
    }

    // Start button for host
    var startBut = document.getElementById("SG");
    if(startBut){
        startBut.onclick = (function(){
            var mersas = document.getElementById("MA").checked;
            var perc = document.getElementById("Pc").checked;
            var mord = document.getElementById("Md").checked;
            var morg = document.getElementById("Mg").checked;
            var ober = document.getElementById("Ob").checked;
            // console.log(mersas + " " + perc + " " + mord + " " + morg + " " + ober);

            if(perc){
                mersas = true;
            } else if(mord) {
                mersas = true;
            } else if(morg) {
                mersas = true;
            } else if(ober) {
                mersas = true;
            }
            gameName = sessionStorage.getItem('gname');
            $.get(WEB_APP, {
                "gname": gameName,
                "mersas": mersas,
                "perc": perc,
                "mord": mord,
                "morg": morg,
                "ober": ober
            }).done(startGame).fail(ajaxFail);
        });
    }

    
};

var startGame = function(data){
    if(data == "yes") {
        document.getElementById('hide').style.display = "none";
    } else {
        swal({
            title: "Error",
            text: data,
            type: "error",
            timer: 4000,
            // showConfirmButton: false
        });
    }
};

var begin = function(h1){
    username = sessionStorage.getItem('User');
    h1.innerHTML = username;
    var host = sessionStorage.getItem('host');
    gameName = sessionStorage.getItem('gname');
    var gnamehead=document.getElementById('gnameHeader');
    gnamehead.innerHTML = gameName;
    gnamehead.className += " center";
    // console.log(host);
    if(host.trim() == "yes") {
        // console.log("here");
        document.getElementById('hide').style.display='block';
    } else {
        var h2 = document.createElement("h2");
        var node = document.createTextNode("Waiting on host to start game...");
        h2.appendChild(node);
        h2.id = "waiting";
        h2.className += " center";
        var div = document.getElementById("middle");
        div.appendChild(h2);
    }
    //
    // add listening for host to start game
    // 
    fetching();
};

// checks db if host has started game
function fetching(){
    $.get(WEB_APP, {
        "fetch": gameName
    }).done(phase2).fail(ajaxFail);
}

var phase2 = function(data){
    if(data == "started") {
        $.get(WEB_APP, {
            "phase2": gameName,
            "username": username
        }).done(showChar).fail(ajaxFail);
    } else {
        setTimeout(fetching, 300);
    }
};

var showChar = function(data) {
    // gets the returned data
    var temp = data.split("-");
    var charName = temp[0];
    var desc = temp[1];
    side = temp[2];
    if(side === 1) {
        side = "good";
    } else {
        side = "bad";
    }

    // updates the character header
    var charHname = document.getElementById("charHeader");
    charHname.innerHTML = charName;
    charHname.style.display = "block";

    // gets rid of waiting for...
    var die = document.getElementById("waiting");
    if(die != null)
        die.style.display = "none";

    // puts the picture up
    charName = charName.toLowerCase();
    console.log(charName);
    var img = document.getElementById("charPic");
    img.style.display = "inline-block";
    img.src = "./images/" + charName + ".png";
    img.style.width = "10em";
    img.style.height = "17em";

    // puts the description up
    var descript = document.getElementById("charDesc"); 
    descript.innerHTML = desc;
    descript.style.backgroundColor = "#ccf2ff";

    showSpecialDesc();
    // voteFunctions();
};

// // assigns the onclick of the voting images but makes them disabled for now
// function voteFunctions(){
//     // success button
//     succ = document.getElementById("succ");
//     succ.onclick = (function(){
//         $.get(WEB_APP, {
//             "succORfail": username
//         });
//     });

//     // fail button
//     fail = document.getElementById("fail");
//     if(side == "bad") {
//         fail.onclick = (function(){
//             $.get(WEB_APP, {
//                 "succORfail": username
//             });
//         });
//     } else {
//         fail.style.opacity = 0.4;
//         fail.disabled = true;
//     }

//     // // approve button
//     // appr = document.getElementById("appr");
//     // appr.onclick = (function(){
//     //     $.get(WEB_APP, {
//     //         "apprORrej": username,
//     //         "TorF": 1
//     //     });
//     // });

//     // // reject button
//     // rej = document.getElementById("rej");
//     // rej.onclick = (function(){
//     //     $.get(WEB_APP, {
//     //         "apprORrej": username,
//     //         "TorF": 0
//     //     });
//     // });
// }

// show Merlin and evil other users
function showSpecialDesc(){
    $.get(WEB_APP, {
        "spec": username,
        "gname": gameName
    }).done(makeTheView).fail(ajaxFail);
}

// makes the alert of spec char abilities
var makeTheView = function(data){
    $.get(WEB_APP, {
        "numOnQuest": gameName
    }).done(getNumOnQuest).fail(ajaxFail);

    var listItems = data.split("-");
    var listItemsString = listItems.join("\n");
    if (listItems[0] !== "") {
        var viewBut = document.getElementById("swalBut");
        viewBut.style.display = "inline-block";
        viewBut.onclick = (function(){
            swal({
                title: "View",
                text: listItemsString
            });
            viewBut.style.display = "none";
        });
    }
};

// finds out how many go on a quest
var getNumOnQuest = function(data){
    $.get(WEB_APP, {
        "leader": username,
        "gameName": gameName
    }).done(round).fail(ajaxFail);

    numOnQuest = data;
};

// starts a round
var round = function(data){
    var temp = data.split("-");
    if(temp[0] == ""){
        leader = "no";
    } else {
        leader = "yes";
    }
    if(leader == "yes"){
        document.getElementById("qpii").innerHTML = numOnQuest;
        for(var i = 0; i<temp.length; i++){
            document.getElementById((i).toString()).style.display = "inline-block";
            document.getElementById("lcheckbox" + (i).toString()).innerHTML = temp[i];
            parseInt(i);
        }

        // checks if leader clicked right number of people
        // if so submits those people to db
        var peepToSubmit = [];
        document.getElementById("SoQ").onclick = (function(){
            var count = 0;
            for(var i = 0; i < temp.length; i++){
                if(document.getElementById((i).toString()).checked) {
                    count++;
                    peepToSubmit.push(document.getElementById("lcheckbox" + (i).toString()).innerHTML);
                }
            }
            if(numOnQuest == 2){
                peepToSubmit.push("");
                peepToSubmit.push("");
                peepToSubmit.push("");
            }
            if(numOnQuest == 3){
                peepToSubmit.push("");
                peepToSubmit.push("");
            }
            if(numOnQuest == 4){
                peepToSubmit.push("");
            }
            if(count == numOnQuest){
                document.getElementById("leaderPower").style.display = "none";
                $.get(WEB_APP, {
                    "SoQ": gameName,
                    "name1": peepToSubmit[0],
                    "name2": peepToSubmit[1],
                    "name3": peepToSubmit[2],
                    "name4": peepToSubmit[3],
                    "name5": peepToSubmit[4]
                }).done(waitToVote).fail(ajaxFail);                
            } else {
                swal({
                    title: "Error",
                    text: "Wrong number of people for the quest.",
                    type: "error",
                    timer: 3000
                });
            }
        });
        document.getElementById("leaderPower").style.display = "block";
    } else {
        for(var i = 0; i<temp.length; i++){
            document.getElementById(i.toString()).style.display = "hide";
        }
        document.getElementById("leaderPower").style.display = "none";
        waitToVote();
    }
};

// asks the db if its time to appr/rej
var waitToVote = function(){
    $.get(WEB_APP, {
        "wTV": gameName
    }).done(checkForPhase3).fail(ajaxFail);
};

var checkForPhase3 = function(data){
    // this means that the user will need to approve or reject
    if(data !== ""){
        var temp = data.split("-");
        if(temp[0] == undefined || temp[1] == undefined || temp[2] == undefined || temp[3] == undefined || temp[4] == undefined) {
            waitToVote();
            return;
        }
        if(numOnQuest == 2){
            var str = "The leader chose " + temp[0].toString() +
                    " and " + temp[1].toString() + " to go on the quest";
        } else if(numOnQuest == 3) {
            var str = "The leader chose " + temp[0].toString() + ", " +
                    temp[1].toString() + ", " +" and " + 
                    temp[2].toString() + " to go on the quest";
        } else if (numOnQuest == 4){
            var str = "The leader chose " + temp[0].toString() + ", " +
                    temp[1].toString() + ", " +
                    temp[2].toString() + ", " +" and " + 
                    temp[3].toString() + " to go on the quest";
        } else if(numOnQuest == 5){
            var str = "The leader chose " + temp[0].toString() + ", " +
                    temp[1].toString() + ", " +
                    temp[2].toString() + ", " +
                    temp[3].toString() + ", " +" and " + 
                    temp[4].toString() + " to go on the quest";
        }
        // this is checking if this user is on the quest
        // if so then it will allow them to do the success
        // or fail vote
        if(temp[0].toString() == username) {
            goingOnQuest = true;
        } else if(temp[1].toString() == username) {
            goingOnQuest = true;
        } else if(temp[2] != undefined && temp[2].toString() == username) {
            goingOnQuest = true;
        } else if(temp[3] != undefined && temp[3].toString() == username) {
            goingOnQuest = true;
        } else if(temp[4] != undefined && temp[4].toString() == username) {
            goingOnQuest = true;
        } 
        // swal for the actual voting
        swal({
            title: "Vote",
            text: str,
            showCancelButton: true,
            animation: "slide-from-top",
            confirmButtonText: "Approve",
            cancelButtonText: "Reject"
        }, function(isConfirm){
            if(isConfirm){
                // approve button
                $.get(WEB_APP, {
                    "apprORrej": username,
                    "gameName": gameName,
                    "TorF": 1
                }).done(waitForAR).fail(ajaxFail);
            } else {
                // reject button
                $.get(WEB_APP, {
                    "apprORrej": username,
                    "gameName": gameName,
                    "TorF": 0
                }).done(waitForAR).fail(ajaxFail);
            }
        });
    } else {
        setTimeout(waitToVote, 1000);
    }
};

// asks the db if everyone has appr/rej
var waitForAR = function(){
    console.log("made it to waiting");
    $.get(WEB_APP, {
        "cV": gameName,
        "num": 0
    }).done(checkForPhase4).fail(ajaxFail);
};

// if in phase 4, puts the results of the vote on the on screen board
var checkForPhase4 = function(data){
    if(data !== ""){
        var groups = data.split("-");
        var countA = 0;
        var countR = 0;
        var board2 = document.getElementById("board2");
        var board3 = document.getElementById("board3");
        while(board3.firstChild){
            board3.removeChild(board3.firstChild)
        }
        while(board2.firstChild){
            board2.removeChild(board2.firstChild)
        }
        for(var i = 0; i < groups.length;i++){
            var grp = groups[i].split("~");
            var pers = grp[0];
            var vote = grp[1];
            if(vote == "1"){
                countA++;
                var li = document.createElement("div");
                li.className += " col-sm-12";
                li.innerHTML = pers;
                document.getElementById("board2").appendChild(li);
            } else if(vote == "0") {
                countR++;
                var li = document.createElement("div");
                li.className += " col-sm-12";
                li.innerHTML = pers;
                document.getElementById("board3").appendChild(li);
            }
        }
        if(countA > countR){
            setTimeout(allegiance(), 2000);
        } else {
            goingOnQuest = false;
            nextQuest = "r";
            setTimeout(restartRound(), 2000);
            // $.get(WEB_APP, {
            //     "delete": gameName
            // }).done(restartRound).fail(ajaxFail);
        }
    } else {
        setTimeout(waitForAR, 200);
    }
};

// resets everything in db and gets ready for next round
var restartRound = function(){
    if(leader == "yes"){
        $.get(WEB_APP, {
            "nextQuest": nextQuest,
            "gameName": gameName,
            "yeaORnea": "yes"
        }).done(deleteVote).fail(ajaxFail);
    } else {
        $.get(WEB_APP, {
            "nextQuest": nextQuest,
            "gameName": gameName,
            "yeaORnea": "no"
        }).done(deleteVote).fail(ajaxFail);
    }
};

// deletes the entry for this game in the Vote table
var deleteVote = function(){
    $.get(WEB_APP, {
        "delete": gameName
    }).done(checkEndGame).fail(ajaxFail);
};

// checks if the game is over
var checkEndGame = function(){
    $.get(WEB_APP, {
        "checkEnd": gameName
    }).done(getLeader).fail(ajaxFail);
};

// finds out who the leader is and then loops back to round
// unless checkEndGame returns that the game is over
var getLeader = function(data){
    if(data == "g"){
        $.get(WEB_APP, {
            "endGame": username,
            "gameName": gameName,
            "ok": leader
        }).fail(ajaxFail);
        swal({
            title: "Game Over",
            text: "Good Won!",
            type: "info"
        },
        function(){
            location.replace("./index.php");
        });
    } else if(data == "bv"){
        $.get(WEB_APP, {
            "endGame": username,
            "gameName": gameName,
            "ok": leader
        }).fail(ajaxFail);
        swal({
            title: "Game Over",
            text: "Bad won from too many rejects.",
            type: "info"
        },
        function(){
            location.replace("./index.php");
        });
    } else if(data == "bw"){
        $.get(WEB_APP, {
            "endGame": username,
            "gameName": gameName,
            "ok": leader
        }).fail(ajaxFail);
        swal({
            title: "Game Over",
            text: "Bad Won!",
            type: "info"
        },
        function(){
            location.replace("./index.php");
        });
    } else if(data == "checkAssas"){
        $.get(WEB_APP, {
            "checkAssas": username,
            "gameName": gameName
        }).done(checkAssas).fail(ajaxFail);
    } else {
        $.get(WEB_APP, {
            "leader": username,
            "gameName": gameName
        }).done(round).fail(ajaxFail);
    }
};

var checkAssas = function(data){
    var temp = data.split("-");
    if(temp[0] == ""){
        assassin = "no";
    } else {
        assassin = "yes";
    }
    if(assassin == "yes"){
        for(var i = 0; i < temp.length; i++){
            document.getElementById("assas" + (i.toString())).style.display = "inline-block";
            document.getElementById("name" + (i.toString())).innerHTML = temp[0];
            parseInt(i);
        }
        document.getElementById("FtW").onclick = (function(){
            var count = 0;
            var victim = "";
            for(var i = 0; i < temp.length; i++){
                if(document.getElementById("assas" + (i).toString()).checked) {
                    count++;
                    victim = document.getElementById("name" + (i.toString())).innerHTML;
                }
            }
            if(count == 1){
                $.get(WEB_APP, {
                    "merlin": victim,
                    "gameName": gameName
                }).done(finish).fail(ajaxFail);
            }
        });
        document.getElementById("assassinForm").style.display = "block";
    } else {
        $.get(WEB_APP, {
            "fetch": gameName
        }).done(finish).fail(ajaxFail);
    }
};

// checks db if assassin has picked a merlin
var fetching2 = function(){
    $.get(WEB_APP, {
        "fetch": gameName
    }).done(finish).fail(ajaxFail);
}

var finish = function(data){
    if(data == "started") {
        setTimeout(fetching2, 500);
    } else {
        $.get(WEB_APP, {
            "endGame": username,
            "gameName": gameName,
            "ok": leader
        }).fail(ajaxFail);
        if(data == "g") {
            swal({
                title: "Game Over",
                text: "Good Won!",
                type: "info"
            },
            function(){
                location.replace("./index.php");
            });
        } else {
            swal({
                title: "Game Over",
                text: "Bad Won!",
                type: "info"
            },
            function(){
                location.replace("./index.php");
            });
        }
    }
};

// checks allegiance of user's character
var allegiance = function(){
    if(goingOnQuest) {
        $.get(WEB_APP, {
            "allegiance": username
        }).done(succOrFail).fail(ajaxFail);
    } else {
        // TODO: have people not on quest wait
    }
};

// allows people on quest to vote success or fail
var succOrFail = function(data){
    if(data == "g"){
        swal({
            title: "Vote",
            text: "Vote for a success or fail. You are a Good player so you can't vote for fail. It is here to try and prevent screen looking being successful.",
            showCancelButton: true,
            animation: "slide-from-top",
            confirmButtonText: "Success",
            cancelButtonText: "Fail",
            closeOnCancel: false
        }, function(isConfirm){
            if(isConfirm){
                // success button
                $.get(WEB_APP, {
                    "apprORrej": username,
                    "gameName": gameName,
                    "TorF": 1
                }).done(finishQuest).fail(ajaxFail);
            }
        });
    } else if(data == "b"){
        swal({
            title: "Vote",
            text: "Vote for a success or fail. You are a Evil player so you can vote for both. It is here to try and prevent screen looking being successful.",
            showCancelButton: true,
            animation: "slide-from-top",
            confirmButtonText: "Success",
            cancelButtonText: "Fail"
        }, function(isConfirm){
            if(isConfirm){
                // approve button
                $.get(WEB_APP, {
                    "apprORrej": username,
                    "gameName": gameName,
                    "TorF": 1
                }).done(finishQuest).fail(ajaxFail);
            } else {
                // reject button
                $.get(WEB_APP, {
                    "apprORrej": username,
                    "gameName": gameName,
                    "TorF": 0
                }).done(finishQuest).fail(ajaxFail);
            }
        });
    }
};

// wait for all success fail votes
var waitForSF = function(){
    $.get(WEB_APP, {
        "cV": gameName,
        "num": numOnQuest
    }).done(finishQuest).fail(ajaxFail);
};

var finishQuest = function(data){
    if(data !== ""){
        $.get(WEB_APP, {
            "getBoard": gameName
        }).done(result).fail(ajaxFail);
        var groups = data.split("-");
        var countS = 0;
        var countF = 0;
        for(var i = 0; i < groups.length;i++){
            var grp = groups[i].split("~");
            var vote = grp[1];
            if(vote == "1"){
                countS++;
            } else if(vote=="0"){
                countF++;
            }
        }
        if(countF == 0){
            nextQuest = "g";
        } else {
            nextQuest = "b";
        }
        document.getElementById("board4").innerHTML = countS;
        document.getElementById("board5").innerHTML = countF;        
    } else {
        setTimeout(waitForSF, 200)
    }
};

var result = function(data){
    var temp = data.split("-");
    document.getElementById("board0").innerHTML = temp[0];
    document.getElementById("board1").innerHTML = temp[1];
    restartRound();
};

var signUp = function(data){
    if(data == "yes") {
        document.getElementById("inputEmail").value = "";
        document.getElementById("inputUser").value = "";
        document.getElementById("inputPass").value = "";
        swal({
            title: "Success!",
            text: "Account created successfully.",
            type: "success",
            timer: 2500,
            // showConfirmButton: false
        });
    } else {
        swal({
            title: "Error",
            text: data,
            type: "error",
            timer: 5000,
            // showConfirmButton: false
        });
    }
};

var ajaxFail = function(xhr, status, exception) {
    console.log(xhr, status, exception);
};

var goToBoard = function(data) {
    // console.log(data);
    if(data == "yes") {
        sessionStorage.setItem('User', username);
        sessionStorage.setItem('host', "no");
        sessionStorage.setItem('gname', gameName);
        // window.location.href = "http://localhost/PHP-280/DatabaseProj/board.php#"+username;
        location.replace("./board.php");
    } else {
        swal({
            title: "Error",
            text: data,
            type: "error",
            timer: 5000,
            // showConfirmButton: false
        });
    }
};

var createGame = function(data){
    if(data == "yes") {
        sessionStorage.setItem('User', username);
        sessionStorage.setItem('host', "yes");
        sessionStorage.setItem('gname', gameName);
        // window.location.href = "http://localhost/PHP-280/DatabaseProj/board.php#"+username;
        location.replace("./board.php");
    } else {
        swal({
            title: "Error",
            text: data,
            type: "error",
            timer: 5000,
            // showConfirmButton: false
        });
    }
};

