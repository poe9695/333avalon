<!DOCTYPE html>
<html>
<head>
    <title>Avalon-Board</title>
    <script src="sweetalert-master/dist/sweetalert.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
      

    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="app.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="app.js"></script>
</head>
<body>
    <div id="board">
        <img class="castle" src="./images/castle2.jpg">
        <div class="col-lg-4">

            <h1 id="gnameHeader" class="text-primary"></h1>
        <?php
            //          0               1               2                       3                   4           5
            $headers = ["Good Team", "Bad Team", "Approves the Quest", "Rejects the Quest", "Good Votes", "Bad Votes"];
            for($i = 0; $i < 6; $i++) {
        
            if($i%2 == 0) {
                ?>
                <div class="row">
                <?php
            }
        ?>
            <div class="col-xs-6">
            <?php
                if($i%2 == 0) {
                ?>
                    <div class="panel panel-info offTop">
                        <div class="panel-heading">
                            <h2 class="panel-title"><?=$headers[$i]?></h2>
                        </div>
                        <div class="panel-body">
                            <div id="board<?=$i?>">
                            </div>
                        </div>
                    </div>
                <?php
                } else {
                ?>
                    <div class="panel panel-info offTop">
                        <div class="panel-heading">
                            <h2 class="panel-title"><?=$headers[$i]?></h2>
                        </div>
                        <div class="panel-body">
                            <div id="board<?=$i?>">
                            </div>
                        </div>
                    </div>
                <?php
                }
            ?>
            </div>
        <?php
            if($i%2 == 1) {
                ?>
                </div>
                <?php
            }
            }
        ?>  <div class="row">
                <div class="col-xs-4 col-xs-offset-4">
                    <div class="panel panel-info offTop">
                            <div class="panel-heading">
                                <h2 class="panel-title">Rejected Rounds</h2>
                            </div>
                            <div class="panel-body">
                                <div id="rejCount">
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        <!-- 
            <div class="col-sm-3">
                <img id="succ" class="sameSizeNA" src="./images/success.png">
            </div>
            <div class="col-sm-3">
                <img id="fail" class="sameSizeNA" src="./images/fail.png">                
            </div>
            <div class="col-sm-3">
                <img id="appr" class="sameSizeNA" src="./images/approve.png">                
            </div>
            <div class="col-sm-3">
                <img id="rej" class="sameSizeNA" src="./images/reject.png">                
            </div> -->
        </div>
        <div id="middle" class="col-lg-4">
            <div class="row" id="hide" hidden>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="well" id="colorful">
                        <!-- <form class="form-horizontal"> -->
                            <fieldset>
                                <legend>Host</legend>
                                <p>Once all of the users have joined, check below which
                                    special characters you want. Then click on the start
                                    game button. <br/>
                                    <em>Note:</em> Merlin and Assassin have to be included
                                    if you want any other characters.
                                </p>
                                <?php
                                $ids = ["MA", "Pc", "Md", "Mg", "Ob"];
                                $specChars = ["Merlin and Assassin", "Percival", "Mordred", "Morgana", "Oberon"];
                                for($i=0; $i<5; $i++){
                                ?>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <input id="<?=$ids[$i]?>" type="checkbox"> <?=$specChars[$i]?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>

                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <button type="submit" id="SG" class="btn btn-primary">Start Game</button>
                                    </div>
                                </div>
                            </fieldset>
                        <!-- </form> -->
                    </div>
                </div>
                <!-- Intentionally left blank to please bootstrap 12 columns -->
                <div class="col-sm-2">
                    <span></span>
                </div>
            </div>
            <div class="row" id="leaderPower" hidden>
                <div class="col-xs-4 right">
                    <img src="./images/leaderChip.png" alt="leader chip" id="leader"/>
                </div>
                <div class="col-xs-8">
                    <div class="well" id="colorful">
                        <!-- <form class="form-horizontal"> -->
                            <fieldset>
                                <legend>Leader</legend>
                                <p> You are the leader.  Pick which users you want
                                    to go on the quest.
                                    <br/>
                                    <em>Note:</em> there has to be <span id="qpii"></span> <!--quest people in it-->
                                    people on this quest.
                                </p>
                                <?php
                                for($i=0; $i<10; $i++){
                                ?>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <input id="<?=$i?>" type="checkbox" hidden><span id="lcheckbox<?=$i?>"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                                                <!--Send on Quest-->
                                        <button type="submit" id="SoQ" class="btn btn-primary">Send on Quest!</button>
                                    </div>
                                </div>
                            </fieldset>
                        <!-- </form> -->
                    </div>
                </div><!-- 
                <div class="col-lg-">
                    
                </div> -->
            </div>
            <div class="row" id="assassinForm" hidden>
                <div class="col-sm-8 col-sm-offset-2">
                    <div class="well" id="colorful">
                        <!-- <form class="form-horizontal"> -->
                            <fieldset>
                                <legend>Assassin Power</legend>
                                <p> Evil has lost. However it is in your power to kill
                                    Merlin. If you guess which user is Merlin, you 
                                    will change the tide and Evil will win!
                                </p>
                                <?php
                                for($i=0; $i<7; $i++){
                                ?>
                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <div class="checkbox">
                                            <label>
                                                <input id="assas<?=$i?>" type="checkbox" hidden><span id="name<?=$i?>"></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                }
                                ?>

                                <div class="form-group">
                                    <div class="col-lg-10 col-lg-offset-2">
                                        <button type="submit" id="SG" class="btn btn-primary">For the Win!</button>
                                    </div>
                                </div>
                            </fieldset>
                        <!-- </form> -->
                    </div>
                </div>
                <!-- Intentionally left blank to please bootstrap 12 columns -->
                <div class="col-sm-2">
                    <span></span>
                </div>
            </div>
        </div>
        <div id="rightSide" class="col-lg-4">
            <h1 class="center text-primary" id="headUsername"></h1>
            <h2 class="center" id="charHeader" hidden></h2>
            <!-- <div class="row">
                
            </div> -->
            <div class="row">
                <div class="col-sm-5 center">
                    <img src="" id="charPic" />
                </div>
                <div class="col-sm-4" id="charDesc">
                    <!-- This is merlin.  He is very powerful and needs a long description.
                    Since his description is so long, it will help out my testing. I
                    need to make it look good for the presentation tomorrow. It really 
                    sucks because I feel like frontend is farther behind than the backend
                    but what do I know. -->
                </div>
                <div class="col-sm-3">
                    <button class="btn btn-primary" id="swalBut">Show Me</button>
                </div>
            </div>
        </div>


<!-- 
        <div class="col-xs-6">
            <div id="leftSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Good Team</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div id="rightSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Bad Team</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>
        </div>
        <div class="col-xs-6">
            <div id="leftSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Votes for the Mission</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>            
        </div>
        <div class="col-xs-6">
            <div id="rightSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Votes Against the Mission</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>            
        </div>
        <div class="col-xs-6">
            <div id="leftSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Good Votes</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>            
        </div>
        <div class="col-xs-6">
            <div id="rightSide" class="panel panel-info">
                <div class="panel-heading">
                    <h2 class="panel-title">Bad Votes</h2>
                </div>
                <div class="panel-body">
                    <h3>0</h3>
                </div>
            </div>            
        </div> -->
    </div>
</body>
</html>