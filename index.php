<!DOCTYPE html>
<html>
<head>
    <title>Avalon</title>
    <!-- Downloaded from github sweet alert bootstrap -->
    <script src="sweetalert-master/dist/sweetalert.min.js"></script> 
    <link rel="stylesheet" type="text/css" href="sweetalert-master/dist/sweetalert.css">
            

    <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="app.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script type="text/javascript" src="app.js"></script>
</head>
<body>
    <div id="board">
        <img class="castle" src="./images/castle2.jpg">
        <div id="page">
            <h1 class="center text-primary">Avalon</h1>
            <div class="col-sm-6">
                <div class="well">
                    <!-- <form class="form-horizontal"> -->
                        <fieldset>
                            <legend>Login</legend>
                            <div class="form-group">
                                <label for="inputUser" class="col-lg-2 control-label">Username</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="inputUserLogin" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPass" class="col-lg-2 control-label">Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="inputPassLogin" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputGName" class="col-lg-2 control-label">Game Name</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="inputGNameLogin" placeholder="Name of Game">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button type="submit" id="CG" class="btn btn-primary">Create Game</button>
                                    <button type="submit" id="JG" class="btn btn-primary">Join Game</button>
                                </div>
                            </div>
                        </fieldset>
                    <!-- </form> -->
                </div>
            </div>
            <div class="col-sm-6">
                <div class="well">
                    <!-- <form class="form-horizontal"> -->
                        <fieldset>
                            <legend>Sign Up</legend>
                            <div class="form-group">
                                <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="inputEmail" placeholder="Email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputUser" class="col-lg-2 control-label">Username</label>
                                <div class="col-lg-10">
                                    <input type="text" class="form-control" id="inputUser" placeholder="Username">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPass" class="col-lg-2 control-label">Password</label>
                                <div class="col-lg-10">
                                    <input type="password" class="form-control" id="inputPass" placeholder="Password">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-lg-10 col-lg-offset-2">
                                    <button class="btn btn-primary" id="SU">Sign Up</button>
                                </div>
                            </div>
                        </fieldset>
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>

    
    
</body>
</html>